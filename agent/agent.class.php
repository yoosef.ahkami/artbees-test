<?php
	
	class Custom_agent_main
	{
		public function __construct()
		{
			define( 'AGENT_BASE_URL', BEANS_THEME_URL . 'agent/' );
			beans_add_smart_action( 'wp_enqueue_scripts', array ( $this, 'bean_agent_assets' ), 5 );
			beans_add_smart_action( 'admin_enqueue_scripts', array ( $this, 'bean_agent_admin_assets' ), 5 );
			beans_add_smart_action( 'init', array ( $this, 'registr_custom_post_type' ), 5 );
			beans_add_smart_action( 'admin_init', array ( $this, 'register_agent_meta' ), 5 );
			beans_add_smart_action( 'wp_ajax_nopriv_add_agent_info', array ( $this, 'add_agent' ) );
			beans_add_smart_action( 'wp_ajax_add_agent_info', array ( $this, 'add_agent' ) );
			beans_add_smart_action( 'after_switch_theme', array ( $this, 'create_agent_menu' ), 5 );
		}
		
		/**
		 * ------------------------------------------------------------------------------------------
		 *  Register all front-end required scripts
		 * ------------------------------------------------------------------------------------------
		 */
		public function bean_agent_assets()
		{
			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_style( 'custom-jquery-ui', AGENT_BASE_URL . 'assets/css/jquery-ui.min.css' );
			wp_enqueue_style( 'custom-agent-style', AGENT_BASE_URL . 'assets/css/style.css' );
			wp_enqueue_script( 'custom-agent-js', AGENT_BASE_URL . 'assets/js/agent.min.js', array ( 'jquery' ) );
			wp_localize_script( 'custom-agent-js', 'custom_agent', array (
				'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
			) );
		}
		
		/**
		 * ------------------------------------------------------------------------------------------
		 *  Register all back-end required scripts
		 * ------------------------------------------------------------------------------------------
		 */
		public function bean_agent_admin_assets()
		{
			wp_enqueue_script( 'jquery-ui-datepicker' );
			wp_enqueue_script( 'jquery-ui-autocomplete' );
			wp_enqueue_style( 'custom-jquery-ui', AGENT_BASE_URL . 'assets/css/jquery-ui.min.css' );
			wp_enqueue_script( 'custom-agent-js', AGENT_BASE_URL . 'assets/js/agent.min.js', array ( 'jquery' ) );
			wp_localize_script( 'custom-agent-js', 'custom_agent', array (
				'ajaxurl' => esc_url( admin_url( 'admin-ajax.php' ) ),
			) );
		}
		
		/**
		 * ------------------------------------------------------------------------------------------
		 *  Register required custom post_type
		 * ------------------------------------------------------------------------------------------
		 */
		public function registr_custom_post_type()
		{
			register_post_type( 'agent', array (
				'label'               => esc_html__( 'Agents', 'tm-beans' ),
				'description'         => esc_html__( 'Manage the agents in this section.', 'tm-beans' ),
				'exclude_from_search' => true,
				'public'              => true,
				'has_archive'         => true,
				'rewrite'             => array ( 'slug' => 'agent' ),
				'supports'            => array ( 'title' ),
				'menu_icon'           => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiIHZpZXdCb3g9IjAgMCA4MC4xMyA4MC4xMyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgODAuMTMgODAuMTM7IiB4bWw6c3BhY2U9InByZXNlcnZlIj48Zz48cGF0aCBkPSJNNDguMzU1LDE3LjkyMmMzLjcwNSwyLjMyMyw2LjMwMyw2LjI1NCw2Ljc3NiwxMC44MTdjMS41MTEsMC43MDYsMy4xODgsMS4xMTIsNC45NjYsMS4xMTIgICBjNi40OTEsMCwxMS43NTItNS4yNjEsMTEuNzUyLTExLjc1MWMwLTYuNDkxLTUuMjYxLTExLjc1Mi0xMS43NTItMTEuNzUyQzUzLjY2OCw2LjM1LDQ4LjQ1MywxMS41MTcsNDguMzU1LDE3LjkyMnogTTQwLjY1Niw0MS45ODQgICBjNi40OTEsMCwxMS43NTItNS4yNjIsMTEuNzUyLTExLjc1MnMtNS4yNjItMTEuNzUxLTExLjc1Mi0xMS43NTFjLTYuNDksMC0xMS43NTQsNS4yNjItMTEuNzU0LDExLjc1MlMzNC4xNjYsNDEuOTg0LDQwLjY1Niw0MS45ODQgICB6IE00NS42NDEsNDIuNzg1aC05Ljk3MmMtOC4yOTcsMC0xNS4wNDcsNi43NTEtMTUuMDQ3LDE1LjA0OHYxMi4xOTVsMC4wMzEsMC4xOTFsMC44NCwwLjI2MyAgIGM3LjkxOCwyLjQ3NCwxNC43OTcsMy4yOTksMjAuNDU5LDMuMjk5YzExLjA1OSwwLDE3LjQ2OS0zLjE1MywxNy44NjQtMy4zNTRsMC43ODUtMC4zOTdoMC4wODRWNTcuODMzICAgQzYwLjY4OCw0OS41MzYsNTMuOTM4LDQyLjc4NSw0NS42NDEsNDIuNzg1eiBNNjUuMDg0LDMwLjY1M2gtOS44OTVjLTAuMTA3LDMuOTU5LTEuNzk3LDcuNTI0LTQuNDcsMTAuMDg4ICAgYzcuMzc1LDIuMTkzLDEyLjc3MSw5LjAzMiwxMi43NzEsMTcuMTF2My43NThjOS43Ny0wLjM1OCwxNS40LTMuMTI3LDE1Ljc3MS0zLjMxM2wwLjc4NS0wLjM5OGgwLjA4NFY0NS42OTkgICBDODAuMTMsMzcuNDAzLDczLjM4LDMwLjY1Myw2NS4wODQsMzAuNjUzeiBNMjAuMDM1LDI5Ljg1M2MyLjI5OSwwLDQuNDM4LTAuNjcxLDYuMjUtMS44MTRjMC41NzYtMy43NTcsMi41OS03LjA0LDUuNDY3LTkuMjc2ICAgYzAuMDEyLTAuMjIsMC4wMzMtMC40MzgsMC4wMzMtMC42NmMwLTYuNDkxLTUuMjYyLTExLjc1Mi0xMS43NS0xMS43NTJjLTYuNDkyLDAtMTEuNzUyLDUuMjYxLTExLjc1MiwxMS43NTIgICBDOC4yODMsMjQuNTkxLDEzLjU0MywyOS44NTMsMjAuMDM1LDI5Ljg1M3ogTTMwLjU4OSw0MC43NDFjLTIuNjYtMi41NTEtNC4zNDQtNi4wOTctNC40NjctMTAuMDMyICAgYy0wLjM2Ny0wLjAyNy0wLjczLTAuMDU2LTEuMTA0LTAuMDU2aC05Ljk3MUM2Ljc1LDMwLjY1MywwLDM3LjQwMywwLDQ1LjY5OXYxMi4xOTdsMC4wMzEsMC4xODhsMC44NCwwLjI2NSAgIGM2LjM1MiwxLjk4MywxMi4wMjEsMi44OTcsMTYuOTQ1LDMuMTg1di0zLjY4M0MxNy44MTgsNDkuNzczLDIzLjIxMiw0Mi45MzYsMzAuNTg5LDQwLjc0MXoiIGZpbGw9IiNhMGE1YWEiLz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PGc+PC9nPjxnPjwvZz48Zz48L2c+PC9zdmc+',
			) );
		}
		
		/**
		 * ------------------------------------------------------------------------------------------
		 *  Register agent's meta boxes
		 * ------------------------------------------------------------------------------------------
		 */
		public function register_agent_meta()
		{
			$fields = array (
				array (
					'id'      => 'gender',
					'label'   => esc_html__( 'Gender', 'tm-beans' ),
					'type'    => 'select',
					'options' => array (
						1 => esc_html__( 'Male', 'tm-beans' ),
						2 => esc_html__( 'Female', 'tm-beans' ),
						3 => esc_html__( 'Other', 'tm-beans' ),
					),
					'default' => 1
				),
				array (
					'id'    => 'phone',
					'label' => esc_html__( 'Phone', 'tm-beans' ),
					'type'  => 'text',
				),
				array (
					'id'    => 'email',
					'label' => esc_html__( 'Email', 'tm-beans' ),
					'type'  => 'text',
				),
				array (
					'id'    => 'nationality',
					'label' => esc_html__( 'Nationality', 'tm-beans' ),
					'type'  => 'text',
				),
				array (
					'id'    => 'db',
					'label' => esc_html__( 'Date of Birth', 'tm-beans' ),
					'type'  => 'text',
				),
				array (
					'id'    => 'address',
					'label' => esc_html__( 'Address', 'tm-beans' ),
					'type'  => 'textarea',
				),
				array (
					'id'    => 'edu_bg',
					'label' => esc_html__( 'Education Background', 'tm-beans' ),
					'type'  => 'textarea',
				),
				array (
					'id'      => 'contact_mode',
					'label'   => esc_html__( 'Preferred Contact Mode', 'tm-beans' ),
					'type'    => 'select',
					'options' => array (
						1 => esc_html__( 'Phone', 'tm-beans' ),
						2 => esc_html__( 'Email', 'tm-beans' ),
						3 => esc_html__( 'None', 'tm-beans' ),
					),
					'default' => 1
				),
			);
			
			beans_register_post_meta( $fields, array ( 'agent' ), 'section_id', array ( 'title' => esc_html__( 'Extra Information', 'tm-beans' ) ) );
		}
		
		/**
		 * ------------------------------------------------------------------------------------------
		 *  Add agent function
		 * ------------------------------------------------------------------------------------------
		 */
		public function add_agent()
		{
			$response['status']  = false;
			$response['message'] = esc_html__( 'Please send required information.', 'tm-beans' );
			if ( ! empty( $_POST ) and ! empty( $_POST['agentInfo'] ) )
			{
				$agent_info = array ();
				foreach ( $_POST['agentInfo'] as $agent_info_item )
				{
					$agent_info[ $agent_info_item['name'] ] = $agent_info_item['value'];
				}
				if ( wp_verify_nonce( $agent_info['security'], 'add_agent_security_code' ) )
				{
					$agent_name         = ! empty( $agent_info['agent_name'] ) ? sanitize_text_field( $agent_info['agent_name'] ) : '';
					$agent_gender       = ! empty( $agent_info['agent_gender'] ) ? intval( $agent_info['agent_gender'] ) : '';
					$agent_phone        = ! empty( $agent_info['agent_phone'] ) ? sanitize_text_field( $agent_info['agent_phone'] ) : '';
					$agent_email        = ! empty( $agent_info['agent_email'] ) ? sanitize_email( $agent_info['agent_email'] ) : '';
					$agent_nationality  = ! empty( $agent_info['agent_nationality'] ) ? sanitize_text_field( $agent_info['agent_nationality'] ) : '';
					$agent_db           = ! empty( $agent_info['agent_db'] ) ? sanitize_text_field( $agent_info['agent_db'] ) : '';
					$agent_address      = ! empty( $agent_info['agent_address'] ) ? sanitize_text_field( $agent_info['agent_address'] ) : '';
					$agent_edu_bg       = ! empty( $agent_info['agent_edu_bg'] ) ? sanitize_text_field( $agent_info['agent_edu_bg'] ) : '';
					$agent_contact_mode = ! empty( $agent_info['agent_contact_mode'] ) ? intval( $agent_info['agent_contact_mode'] ) : '';
					
					
					if ( ! empty( $agent_name ) && ! empty( $agent_gender ) && ! empty( $agent_phone ) && ! empty( $agent_email ) && ! empty( $agent_nationality ) && ! empty( $agent_db ) && ! empty( $agent_address ) && ! empty( $agent_contact_mode ) )
					{
						$post_info        = array (
							'post_title'  => $agent_name,
							'post_type'   => 'agent',
							'post_status' => 'publish'
						);
						$inserted_post_id = wp_insert_post( $post_info );
						
						if ( $inserted_post_id )
						{
							$post_meta_array = array (
								array (
									'id'    => 'gender',
									'value' => $agent_gender
								),
								array (
									'id'    => 'phone',
									'value' => $agent_phone
								),
								array (
									'id'    => 'email',
									'value' => $agent_email
								),
								array (
									'id'    => 'nationality',
									'value' => $agent_nationality
								),
								array (
									'id'    => 'db',
									'value' => $agent_db
								),
								array (
									'id'    => 'address',
									'value' => $agent_address
								),
								array (
									'id'    => 'edu_bg',
									'value' => $agent_edu_bg
								),
								array (
									'id'    => 'contact_mode',
									'value' => $agent_contact_mode
								)
							);
							
							foreach ( $post_meta_array as $field )
							{
								update_post_meta( $inserted_post_id, $field['id'], $field['value'] );
							}
							$response['status']  = true;
							$response['message'] = esc_html__( 'Thanks for your registration.', 'tm-beans' );
						}
						else
						{
							$response['status']  = false;
							$response['message'] = esc_html__( 'Because of some technical issue we cannot add any data.', 'tm-beans' );
						}
					}
					else
					{
						$response['status']  = false;
						$response['message'] = esc_html__( 'Please fill all the required fields.', 'tm-beans' );
					}
				}
				else
				{
					$response['status']  = false;
					$response['message'] = esc_html__( 'Are you kidding?', 'tm-beans' );
				}
			}
			echo json_encode( $response );
			die();
		}
		
		/**
		 * ------------------------------------------------------------------------------------------
		 *  Create Required menus
		 * ------------------------------------------------------------------------------------------
		 */
		public function create_agent_menu()
		{
			
			$menu_name_name   = 'Navigation';
			$main_menu        = wp_get_nav_menu_object( $menu_name_name );
			$menu_id          = ! $main_menu ? wp_create_nav_menu( $menu_name_name ) : $main_menu->term_id;
			$menu_items       = wp_get_nav_menu_items( $menu_id );
			$menu_pre_defined = false;
			if ( $menu_items )
			{
				foreach ( $menu_items as $menu_item )
				{
					if ( $menu_item->post_title === 'Agents' )
					{
						$menu_pre_defined = true;
						break;
					}
				}
			}
			if ( ! $menu_pre_defined )
			{
				wp_update_nav_menu_item( $menu_id, 0, array (
					'menu-item-title'  => esc_html__( 'Agents', 'tm-beans' ),
					'menu-item-url'    => home_url( '?post_type=agent' ),
					'menu-item-status' => 'publish'
				) );
			}
		}
	}