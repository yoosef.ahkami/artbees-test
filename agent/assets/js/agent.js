jQuery( document ).ready( function () {
	var addAgentBox = jQuery( '#add-agent-form-container' ),
		agentForm   = addAgentBox.find( '.agent-form ' );
	if ( jQuery.isFunction( jQuery.fn.datepicker ) )
	{
		jQuery( ".datepicker, input[name='beans_fields[db]']" ).datepicker( {
			dateFormat:  "yy-mm-dd",
			changeMonth: true,
			changeYear:  true

		} );
	}
	if ( jQuery.isFunction( jQuery.fn.autocomplete ) )
	{
		var nationalities    = ["Afghan", "Albanian", "Algerian", "American", "Andorran", "Angolan", "Antiguans", "Argentinean"],
			nationalityField = jQuery( "#agent_nationality, input[name='beans_fields[nationality]']" );
		nationalityField.autocomplete( {
			source: nationalities
		} );
		nationalityField.autocomplete( "option", "appendTo", agentForm );
	}
	agentForm.on( 'submit', function ( e ) {
		e.preventDefault();
		var fields     = jQuery( this ).find( 'input, select, textarea' ),
			error      = false,
			messageBox = jQuery( '#form-message-box' ).children().addClass( 'uk-hidden uk-alert-success uk-alert-success' ).text( '' );

		fields.each( function ( i ) {
			var _this = jQuery( this );
			if ( _this.data( 'required' ) )
			{
				!jQuery( this ).val() ? ( jQuery( this ).addClass( 'alert' ).next( '.uk-alert' ).removeClass( 'uk-hidden' ), error = true ) : jQuery( this ).removeClass( 'alert' ).next( '.uk-alert' ).addClass( 'uk-hidden' );
			}
		} );

		if ( !error )
		{
			jQuery( this ).siblings( '.loader-box' ).removeClass( 'uk-hidden' );
			jQuery.ajax( {
				type:     "POST",
				url:      custom_agent.ajaxurl,
				dataType: "json",
				data:     {
					action:    "add_agent_info",
					agentInfo: jQuery( this ).serializeArray()
				}
			} ).done( function ( data ) {
				console.log( data );
				if ( data.status )
				{
					messageBox.addClass( 'uk-alert-success' );
					location.reload();
				}
				else
				{
					messageBox.addClass( 'uk-alert-success' );
				}
				jQuery( this ).siblings( '.loader-box' ).addClass( 'uk-hidden' );
				messageBox.removeClass( 'uk-hidden' ).text( data.message );
			} );
		}
	} )
} );