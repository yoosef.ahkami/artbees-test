<?php
	get_header();
	add_thickbox();
?>
	<div id="agent-list-tbl">
		<?php
			if ( is_user_logged_in() )
			{
				?>
				<a href="#TB_inline?width=800&height=700&inlineId=add-agent-form-container" class="thickbox uk-button uk-button-large uk-button-primary uk-float-right add-agent"><?php esc_html_e( 'Add New Agent', 'tm-beans' ) ?></a>
				<?php
			}
			if ( have_posts() )
			{
				$i          = 1;
				$agent_info = array ();
				$per_page   = get_option( 'posts_per_page' );
				$paged      = get_query_var( 'paged', 1 );
				$start_i    = ! empty( $paged ) ? ( ( $paged - 1 ) * $per_page ) + 1 : 1;
				?>
				<table class="uk-table uk-table-hover uk-table-striped">
					<thead>
					<tr>
						<th>#</th>
						<th><?php esc_html_e( 'First Name', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Gender', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Phone', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Email', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Nationality', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Date of Birth', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Address', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Education background', 'tm-beans' ) ?></th>
						<th><?php esc_html_e( 'Preferred Contact Mode', 'tm-beans' ) ?></th>
						<?php
							if ( is_user_logged_in() )
							{
								?>
								<th></th>
								<?php
							}
						?>
					</tr>
					</thead>
					<tbody>
					<?php
						while ( have_posts() )
						{
							the_post();
							$agent_id         = get_the_ID();
							$name             = get_the_title();
							$edit_page_url    = get_edit_post_link( $agent_id );
							$gender_val       = beans_get_post_meta( 'gender' );
							$phone            = beans_get_post_meta( 'phone' );
							$email            = beans_get_post_meta( 'email' );
							$nationality      = beans_get_post_meta( 'nationality' );
							$db               = beans_get_post_meta( 'db' );
							$address          = beans_get_post_meta( 'address' );
							$edu_bg           = beans_get_post_meta( 'edu_bg' );
							$contact_mode_val = beans_get_post_meta( 'contact_mode' );
							
							switch ( $gender_val )
							{
								case 2:
									$gender = esc_html__( 'Female', 'tm-beans' );
									break;
								case 3:
									$gender = esc_html__( 'Other', 'tm-beans' );
									break;
								default:
									$gender = esc_html__( 'Male', 'tm-beans' );
									break;
							}
							switch ( $contact_mode_val )
							{
								case 2:
									$contact_mode = esc_html__( 'Email', 'tm-beans' );
									break;
								case 3:
									$contact_mode = esc_html__( 'None', 'tm-beans' );
									break;
								default:
									$contact_mode = esc_html__( 'Phone', 'tm-beans' );
									break;
							}
							?>
							<tr>
								<td><?php echo $start_i ?></td>
								<td><?php echo $name ?></td>
								<td><?php echo $gender ?></td>
								<td><?php echo $phone ?></td>
								<td><?php echo $email ?></td>
								<td><?php echo $nationality ?></td>
								<td><?php echo $db ?></td>
								<td><?php echo $address ?></td>
								<td><?php echo $edu_bg ?></td>
								<td><?php echo $contact_mode ?></td>
								<?php
									if ( is_user_logged_in() )
									{
										?>
										<td>
											<a href="<?php echo esc_url( $edit_page_url ) ?>" target="_blank">
												<i class="uk-icon-edit "></i>
											</a>
										</td>
										<?php
									}
								?>
							</tr>
							<?php
							$start_i ++;
						}
					?>
					</tbody>
				</table>
				<?php
				beans_posts_pagination();
			}
			if ( is_user_logged_in() )
			{
				?>
				<section class="main-container" style="display: none;" id="add-agent-form-container">
					<h2><?php esc_html_e( 'Registration Information', 'tm-beans' ) ?></h2>
					<div class="loader-box uk-hidden"><i class="uk-icon-spinner uk-icon-spin"></i></div>
					<div class="success-agent-registration uk-hidden">
						<h3><?php esc_html_e( 'Registration is Complete :)', 'tm-beans' ) ?></h3>
					</div>
					<form action="#" name="agent-form" class="main-contact-form agent-form uk-form uk-grid">
						<input type="hidden" name="security" value="<?php echo wp_create_nonce( 'add_agent_security_code' ) ?>">
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_name">
								<?php esc_html_e( 'First Name', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<input class="uk-input" type="text" id="agent_name" name="agent_name" data-required="required">
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please fill this field.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_gender">
								<?php esc_html_e( 'Gender', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<select class="uk-select" id="agent_gender" name="agent_gender" data-required="required">
									<option value="1"><?php esc_html_e( 'Male', 'tm-beans' ) ?></option>
									<option value="2"><?php esc_html_e( 'Female', 'tm-beans' ) ?></option>
									<option value="2"><?php esc_html_e( 'Other', 'tm-beans' ) ?></option>
								</select>
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please fill this field.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_phone">
								<?php esc_html_e( 'Phone', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<input class="uk-input" type="text" id="agent_phone" name="agent_phone" data-required="required">
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please fill this field.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_email">
								<?php esc_html_e( 'Email', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<input class="uk-input" type="email" id="agent_email" name="agent_email" data-required="required">
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please fill this field.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_nationality">
								<?php esc_html_e( 'Nationality', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<input class="uk-input" type="text" id="agent_nationality" name="agent_nationality" data-required="required">
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please fill this field.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_db">
								<?php esc_html_e( 'Date of Birth', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<input class="uk-input datepicker" type="text" id="agent_db" name="agent_db" data-required="required">
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please fill this field.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_address">
								<?php esc_html_e( 'Address', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<textarea class="uk-textarea" id="agent_address" name="agent_address" data-required="required"></textarea>
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please fill this field.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_edu_bg">
								<?php esc_html_e( 'Education background', 'tm-beans' ) ?>:
							</label>
							<div class="uk-form-controls">
								<textarea class="uk-textarea" id="agent_edu_bg" name="agent_edu_bg"></textarea>
							</div>
						</div>
						<div id="form-message-box" class="uk-width-1-1">
							<div class="uk-alert uk-hidden"></div>
						</div>
						<div class="uk-margin uk-width-1-2">
							<label class="uk-form-label" for="agent_contact_mode">
								<?php esc_html_e( 'Preferred Contact Mode', 'tm-beans' ) ?><span>*</span> :
							</label>
							<div class="uk-form-controls">
								<select class="uk-select" id="agent_contact_mode" name="agent_contact_mode" data-required="required">
									<option value="1"><?php esc_html_e( 'Phone', 'tm-beans' ) ?></option>
									<option value="2"><?php esc_html_e( 'Email', 'tm-beans' ) ?></option>
									<option value="3"><?php esc_html_e( 'None', 'tm-beans' ) ?></option>
								</select>
								<div class="uk-hidden uk-alert uk-alert-danger"><?php esc_html_e( 'Please choose one of the items.', 'tm-beans' ) ?></div>
							</div>
						</div>
						<div class="uk-margin btn-container uk-width-1-2">
							<button class="uk-button uk-button-large uk-button-primary" type="submit"><?php esc_html_e( 'Send', 'tm-beans' ) ?></button>
						</div>
					</form>
				</section>
				<?php
			}
		?>
	</div>
<?php
	get_footer();