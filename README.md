# ArtBees Test Theme
This theme is implemented based on the test definitions. Using the Beans, UIkit and WordPress features and APIs is the main feature of this theme.
## Project's Description
In this theme we call the users who want to register "Agent". After theme installation a new menu item will be added in which you can have access to list of agents. If you are logged in, you can register new agent and have access to edit URL of agent post type.
## Poject Folders
To avoid editing the Beans structures and files, we added a new folder in the root of theme which is called "agent" in which added all the required files and folders. In **Assets** folder we included all the required SCSS, CSS and JS files 

## Libraries and APIs
* **WordPress API** : It's used for listing and adding new post type items.
* **WordPress JS Libraries** : For adding modal, we use the WordPress internal library [ThickBox](https://codex.wordpress.org/ThickBox)
* **Beans** : We used the Bean's hooks for our functions and tried to keep the bean's codes safe for future updates.
* **jQuery** : In this project we used jQuery [Datepicker](https://jqueryui.com/datepicker/) and [Autocomplete](https://jqueryui.com/autocomplete/) plugins, so to avoid adding new libraries we
* **Gulp** : To automate some tasks like compiling the SCSS and js files which is not included in theme's package, but you can download it from [here](https://www.dropbox.com/s/pdrt0fsui6x5ofi/gulpfile.js?dl=0) 
* **UIKit** : We tried to use UIKit features as much as possible in this project. But because I prefer SASS in my projects, I added required SCSS files in our this project.    